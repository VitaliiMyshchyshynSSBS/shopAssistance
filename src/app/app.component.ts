import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
   items: Item[] = 
    [
        { purchase: "Макарони", done: false, price: 15.9 },
        { purchase: "Масло", done: false, price: 60 },
        { purchase: "Моолоко", done: true, price: 22.6 },
        { purchase: "Сало", done: false, price:310 }
    ];
    addItem(text: string, price: number): void {
         
        if(text==null || text==undefined || text.trim()=="")
            return;
        if(price==null || price==undefined)
            return;
        this.items.push(new Item(text, price));
    }
}

export class Item{
    purchase: string;
    done: boolean;
    price: number;
     
    constructor(purchase: string, price: number) {
  
        this.purchase = purchase;
        this.price = price;
        this.done = false;
    }
}
